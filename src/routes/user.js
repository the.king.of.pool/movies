
const express= require("express");

const moviesSchema= require("../models/movies");

const router = express.Router();

//create movies

router.post('/movies',(req,res) => {
   const movies= moviesSchema(req.body);
   movies
   .save()
   .then((data) => res.json(data))
   .catch((error) => res.json({message: error}));
});


// get movies,

router.get('/movies',(req,res) => {
    moviesSchema
    .find()
    .then((data) => res.json(data))
    .catch((error) => res.json({message: error}));
 });


 // get movie
 router.get('/movies/:id',(req,res) => {
     const {id}=req.params;
    moviesSchema
    .findById(id)
    .then((data) => res.json(data))
    .catch((error) => res.json({message: error}));
 });

 // update movies
 router.put('/movies/:id',(req,res) => {
    const {id}=req.params;
    const {title,year,released,genere,director,actors,plot,rating}= req.body;
   moviesSchema
   .updateOne({_id:id},{$set:{title,year,released,genere,director,actors,plot,rating}})
   .then((data) => res.json(data))
   .catch((error) => res.json({message: error}));
});

// delete movies
router.delete('/movies/:id',(req,res) => {
    const {id}=req.params;
    
   moviesSchema
   .remove({_id:id})
   .then((data) => res.json(data))
   .catch((error) => res.json({message: error}));
});

module.exports= router;