const express=require('express');

const mongoose= require("mongoose");

require("dotenv").config();

const userRoutes = require("./routes/user");

const app= express();

const port= process.env.PORT || 9000;


// middelware
app.use(express.json());
app.use('/api', userRoutes);



app.get('/',(req,res)=>{

    res.send("Bienvenido");
});

mongoose
.connect(process.env.MONGODB_URI)
.then(() => console.log("Connect Mongo DB Atlas"))
.catch((error) => console.error(error));

app.listen(port,() =>console.log('server escuchanco en puerto', port));