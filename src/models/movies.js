
const mongoose = require("mongoose");

const moviesSchema = mongoose.Schema({

    title:{
        type: String,
        required: true
    },

    year:{
        type: Number,
        required: true
    },
    released:{
        type: String,
        required: true
    },
    genere:{
        type: String,
        required: true
    },
    director:{
        type: String,
        required: true
    },
    actors:{
        type: String,
        required: true
    },
    plot:{
        type: String,
        required: true
    },
    rating:{
        type: Number,
        required: true
    }
});

module.exports= mongoose.model("movies",moviesSchema);